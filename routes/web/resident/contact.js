var Contact = require('../../../models/contact');
var Notification = require('../../../models/notification');
var moment = require('moment');

module.exports = {
	get: function (req, res) {
	var societyId = req.session.user.societyId;
    var userId = req.session.user.id;

		var matcher = {};
	  if (req.session.user.block !== undefined && req.session.user.block !== null && req.session.user.block) matcher.block = req.session.user.block;
	  if (req.session.user.flatNo !== undefined && req.session.user.flatNo !== null && req.session.user.flatNo) matcher.flatNo = req.session.user.flatNo;
	  if (JSON.stringify(matcher) == '{}') { // no filter by block/flatNo (not specified?)
	    matcher = {_id: req.session.user.id}; // only show own documents
	  }

    Contact.find({
      // "openedBy": userId,
      "societyId": societyId
    })
		.populate({
			path: 'openedBy',
			match: matcher,
			select: 'name'
		})
  	.populate('messages.userId', 'name role')
		.exec(function (err, contacts) {
      if (err) {
        console.log(err);
        return res.render('resident/contact',{
          error: true,
          message: "Could not find any result"
        });
      } else {
      	var result = contacts.filter(function (c) {return c.openedBy !== null;})
					.map(function (contact) {
            var robj = {};
            robj.id = contact.id;
            robj.category = contact.category;
            robj.status = contact.status;
            robj.messages = contact.messages;
            robj.dateOpened = contact.dateOpened;
            robj.subject = contact.subject;
            robj.openedBy = contact.openedBy;
            return robj;
          });

        res.render('resident/contact', {
          error: false,
          data: result,
          moment: moment
        });
      }
    });
	},

	reply:  function (req, res) {
    var userId = req.session.user.id;
    var message = req.body.message
    var contactId = req.body.contactId;
    var status = req.body.status;
    Contact.findById(contactId, function (err, contact) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "No contacts found with this Id"
        });
      } else {
      	contact.status = status;
        contact.messages.push({
          "userId": userId,
          "message": message
        });
        contact.save(function (err) {
          if (err) {
            console.log(err);
            return res.json({
              error: true,
              message: "No contacts found with this Id"
            });
          } else {
						// raise notifs
						if (String(contact.openedBy) !== req.session.user.id)	{
							var notif = new Notification({
								content: "You've received a Reply for your Complaint made on " + moment(contact.dateOpened).format("DD/MM/YYYY"),
								for: contact.openedBy
							})
							notif.save(function (err) {
								if (err) {
									console.log('error raising notif');
								} else {
									console.log('raised notif.......');
								}
							});
						}
						// no waiting!
            res.json({
              error: false,
              data: contact
            });
          }
        });
      }
    });
  },

	post: function (req, res) {

		req.body.societyId = req.session.user.societyId;
		req.body.openedBy = req.session.user.id;
		req.body.messages.userId = req.session.user.id;
		console.log(req.body);
		var contact = new Contact(req.body);

    	contact.save(function (err) {
      if (err) {
        console.log(err);
        return res.json({
          error: true,
          message: "Oops! Some error occured"
        });
      } else {
        res.json({
          error: false,
          data: {
            id: contact.id
          }
        });
      }
    });
	}
};
