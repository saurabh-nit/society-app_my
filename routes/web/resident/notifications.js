var Notification = require('../../../models/notification');
var moment = require("moment");

module.exports = {
  get: function (req, res) {
    Notification
    .find({for: req.session.user.id})
    .sort({when: -1})
    .exec()
    .then(function (notifs) {
      // return res.json(notifs);
      res.render("resident/all-alerts", {
        alerts: notifs,
        moment: moment
      })
    })
  },

  post: function (req, res) {
    var userId = req.session.user.id;
    Notification
    .find({for: userId})
    .sort({when: -1})
    .exec()
    .then(function (notifications) {
      return res.json({
        error: false,
        alerts: notifications.slice(0, 5),
        unreadAlertCount: notifications.reduce(function (acc, cur) {
            return (cur.unread) ? acc + 1 : acc;
          }, 0)
      })
    })
    .catch(function (err) {
      return res.json({error: true, reason: err});
    })
  },

  put: function (req, res) {
    var userId = req.session.user.id;
    var notifId = req.params.notifid;
    Notification.update({ _id: notifId }, { $set: { unread: false }}, function (err) {
      if (err) {
        return res.json({error: true, reason: err});
      }
      Notification
      .find({for: userId})
      .exec()
      .then(function (notifications) {
        return res.json({
          error: false,
          alerts: notifications,
          unreadAlertCount: notifications.reduce(function (acc, cur) {
              return (cur.unread) ? acc + 1 : acc;
            }, 0)
        })
      })
      .catch(function (err) {
        return res.json({error: true, reason: err});
      })
    });
  },

}
