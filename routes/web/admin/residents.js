var User = require('../../../models/user');
var Society = require('../../../models/society');

var fs = require('fs');
var randomString = require("randomstring");
// var parse = require('csv-parse');

module.exports = {


  get: function(req, res) {
    var societyId = req.session.user.societyId;

    var filter = {"societyId": societyId, "role": "Resident", "softDeleted": {"$ne": true}};
    var limit = 50; // If NO filters, show just 50

    if (req.query.name) {
      filter.name = new RegExp(req.query.name, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.profession) {
      filter.profession = new RegExp(req.query.profession, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.blockno) {
      filter.block = new RegExp(req.query.blockno, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.flatno) {
      filter.flatNo = new RegExp(req.query.flatno, 'i');
      limit = null; // If a filter is active, there's no limit!
    }
    if (req.query.carno) {
      var carNo = req.query.carno;
      filter.$or = [{'carNumber1': carNo}, {'carNumber2': carNo}, {'carNumber3': carNo}];
      limit = null; // If a filter is active, there's no limit!
    }

    User.find(filter)
      .limit(limit)
      .exec(function (err, residents) {
        if (err) {
          res.json({
            error: true,
            reason: err
          })
        }
        res.render('admin/residents', {
          error: false,
          title: 'Manage Residents',
          residents: residents
        });
      })

  },

  post: function(req, res, next) {
    Society
    .findOne({_id: req.session.user.societyId})
    .exec()
    .then(function (society) {

      User.count({ "societyId": req.session.user.societyId, "softDeleted": {"$ne": true}, "role": "Resident", "block": req.body.block, "flatNo": req.body.flatNo}, function (err0, usersInSameFlat) {
        var isNewFlat = !usersInSameFlat; // If new user is in an existing flat+block, allow it always, else check for pax limit
        society.countReaminingPax(function (err1, paxRem) {
          if (err0 || err1 || ( isNewFlat && paxRem <= 0 )) {
            return res.json({error: true, message: "Approved PAX limit reached! Cannot add any more Residents!"})
          };

          var data = req.body;

          data.societyId = req.session.user.societyId;
          data.addedBy = req.session.user;
          data.role = 'Resident';

          var plainPass = randomString.generate(6); // a random generated password (plaintext)
          data.password = (data.password === undefined) ? plainPass : data.password;

          var resident = new User(data);
          resident.save(function(err) {
            if (err) {
              console.log(err);
              return res.json({
                error: true,
                message: `Specified Email '${data.contactEmail}' already exists in our system! Please choose another.`,
                reason: err
              });
            } else {
              // send password by email
              var locals = {
                to: data.contactEmail,
                // to: 's26c.sayan@gmail.com', // for testing
                subject: 'Welcome to Smart Society!',
                email: data.contactEmail,
                password: data.password,
                name: data.name
              }
              console.log(locals);
              res.mailer.send('emails/password', locals, function (err) {
                if (!err)
                console.log('Password email sent successfully to %s !', data.contactEmail);
                //  res.send('done!');
                else {
                  console.log("error: %o", err);
                  // res.send({error: true})
                }
              });
              // don't wait for email sending to complete
              res.json({
                error: false,
                data: {
                  id: resident.id
                }
              });
            }
          });
        })
      })

    })
    .catch(function (err) {
      return res.json({error: true, message: "No such Society!", reason: err});
    })

  },

  put: function(req, res, next) {
    var data = req.body;
    User
    .findOne({_id: req.params.residentid})
    .exec()
    .then(function (resident) {
      resident.name = data.name;
      resident.block = data.block;
      resident.flatNo = data.flatNo;
      resident.residentType = data.residentType;
      resident.isResiding = data.isResiding;
      resident.contactEmail = data.contactEmail;
      resident.contactPhone = data.contactPhone;
      resident.intercom = data.intercom;

      return resident.save();
    })
    .then(function (saved) {
      return res.json({error: false});
    })
    .catch(function (err) {
      return res.json({error: true, reason: err});
    })
  },

  del: function (req, res) {
    User.update({_id: req.params.residentid}, {$set: {softDeleted: true}}, function (err) {
      if (err) {
        return res.json({error: true, reason: err});
      }
      return res.json({error: false});
    })
  },

  deactivate: function (req, res) {
    User.update({_id: req.params.residentid}, {$set: {status: "inactive"}}, function (err) {
      if (err) {
        return res.json({error: true, reason: err});
      }
      return res.json({error: false});
    })
  },
  reactivate: function (req, res) {
    User.update({_id: req.params.residentid}, {$set: {status: "active"}}, function (err) {
      if (err) {
        return res.json({error: true, reason: err});
      }
      return res.json({error: false});
    })
  },

  postImportCSV: function(req, res) {
    Society
    .findOne({_id: req.session.user.societyId})
    .exec()
    .then(function (society) {
      society.countReaminingPax(function (countingError, remainingPAX) {

        var societyId = req.session.user.societyId;
        var addedBy = req.session.user.id;

        var csvFile = req.file; // thanks to multr middleware
        if (!csvFile) {
          res.json({
            error: true,
            reason: "No File Uploaded!"
          });
        }
        if (csvFile.mimetype != 'text/csv') {
          res.json({
            error: true,
            reason: "Invalid MIME Type"
          });
        }

        var path = csvFile.path;
        var Converter = require("csvtojson").Converter;
        var converter = new Converter({headers: ["block", "flatNo", "name", "residentType", "isResiding", "contactEmail", "contactPhone", "intercom"]});
        converter.fromFile(path, function (err,result) {
          if (err) {
            console.log(err);
          }
          // console.log(result);
          // Filter out those who doesn't have requried fields
          var residents = result.filter(function (resident) {
            return (resident.contactEmail && resident.name);
          });

          if (countingError || residents.length > remainingPAX) {
            if (remainingPAX <= 0) {
              var errMsg = "Approved PAX limit reached! Cannot add any more Residents!";
            } else {
              var errMsg = `You may add only ${remainingPAX} more Residents!`;
            }
            return res.json({error: true, message:  errMsg, reason: countingError});
          }

          // Now "format" the data a bit & optimistically send password mails
          residents = residents.map(function (resident) {
            // delete i.isResiding;
            // delete i.intercom;
            resident.role = 'Resident';
            resident.addedBy = addedBy;
            resident.societyId = societyId;

            resident.isResiding = (resident.isResiding[0].toLowerCase() == 'y') ? true : false;
            resident.password = (resident.password === undefined) ? 'qwerty' : resident.password;

            // if (!resident.contactEmail) resident.contactEmail = resident.name.split(' ')[0].toLowerCase()+'@society.com';

            // send password by email
            // CAVEAT: it may so happen that adding a resident may fail below; but email is still sent!
            var locals = {
              to: resident.contactEmail,
              // to: 's26c.sayan@gmail.com', // for testing
              subject: 'Welcome to Smart Society!',
              email: resident.contactEmail,
              password: resident.password,
              name: resident.name
            }
            res.mailer.send('emails/password', locals, function (err) {
              if (!err)
              console.log('BATCH Password email sent successfully to %s !', resident.contactEmail);
              //  res.send('done!');
              else {
                console.log("BATCH Password email sending error for %s : %o", resident.contactEmail, err);
                // res.send({error: true})
              }
            });

            return resident;
          });


          console.log(residents);
          User.create(residents, function (err) {
            if (err) {
              console.log('error inserting using mongoose %o', err);
              return res.json({error: true, message: "Error Creating at least One Resident"})
            }

            res.json({
              error: false
            })
          })
        });

        // var parser = parse({delimiter: ';'}, function(err, data){
        //   if (err) {
        //     console.log('ERROR!!!! %o', err);
        //   }
        //   console.log("Data: %o", data);
        // });
        //
        // fs.createReadStream(path).pipe(parser);

      })
    })
    .catch(function (err) {
      return res.json({error: true, message: "No such Society!", reason: err});
    })

  },

  resetPassword: function (req, res) {
    var residentId = req.params.residentid;
    var plainPass = randomString.generate(6); // a random generated password (plaintext)
    User
    .findOne({_id: residentId, role: "Resident"})
    .exec()
    .then(function (resident) {
      resident.password = plainPass;
      return resident.save()
    })
    .then(function (savedResident) {
      // send new password by email
      var locals = {
        to: savedResident.contactEmail,
        // to: 's26c.sayan@gmail.com', // for testing
        subject: 'Password Reset!',
        email: savedResident.contactEmail,
        password: plainPass,
        name: savedResident.name
      }
      console.log(locals);
      res.mailer.send('emails/passwordreset', locals, function (err) {
        if (!err)
        console.log('Password change email sent successfully to %s !', savedResident.contactEmail);
        //  res.send('done!');
        else {
          console.log("error: %o", err);
          // res.send({error: true})
        }
      });
      // don't wait for email sending to complete
      return res.json({
        error: false,
        complete: true
      })
    })
    .catch(function (err) {
      return res.json({
        error: true,
        complete: true
      })
    })
  }


}
